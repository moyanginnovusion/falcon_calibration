#!/bin/sh
python3 data_filter_pyramid.py --configure configures/all_points.json --input-prefix $1 --output-prefix $1 --channel 0 --frame 0
python3 data_filter_pyramid.py --configure configures/all_points.json --input-prefix $1 --output-prefix $1 --channel 1 --frame 0
python3 data_filter_pyramid.py --configure configures/all_points.json --input-prefix $1 --output-prefix $1 --channel 2 --frame 0
python3 data_filter_pyramid.py --configure configures/all_points.json --input-prefix $1 --output-prefix $1 --channel 3 --frame 0
