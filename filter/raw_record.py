#
# This script saves a number of frames (for a given frameID) from the input file, including only the data points meeting the (x,y,z) range selection criteria. To use it, first run it without any filtering (filter_on = Flase). An x-y plot will be generated, which can be used for defining the filter. Assuming the pettern consists of four dots, pick the center points of the UL, LL, UR, LR box and size. Then run it again with filter_on to extract those data points and save them in a file. After that, it might be necessary to manually drop some data points that requires more complicated filters (can be done in Excel).
#
# Gang's updated file (with two angles and motor periods)
#
# struct {
# double frame_id;      // 0, 1, 2 or 3 corresponding to 4 bowl facets
# double channel;       // 0 is the top channel, 1 is the bottom channel
# double facet_id;      // start with 0
# double fast_motor_time;     // falling edge
# double slow_motor_time;     // falling edge
# double fast_motor_period;   // lastest updated (ns)
# double slow_motor_period;   // lastest updated (ns)
# double fast_encoder_angle;
# double slow_motor_angle;
# double pulse_time;
# double pulse_distance;
# // Below are the results of Gang's program, for debugging purpose
# double theta;		// polygon_angle, facet norm relative to -z direction
# double phi;			// bowl_angle, zero when the norm of the top mirror has no y component
# double x;			//
# double y;
# double z;
# double intensity;
# }
#


import struct

# raw_record_in_file_size = 17 * 8 + 32 * 2
# raw_record_in_file_size = 20 * 8 + 32 * 2
raw_record_in_file_size = 21 * 8 + 32 * 2


class RawRecord:
    def __init__(self, rec):
        self.frame = int(rec[0])
        self.channel = int(rec[1])
        self.facet = int(rec[2])
        self.fast_t = rec[3]  # fast, slow and pulse times are all in nano second
        self.slow_t = rec[4]
        self.fast_prd = rec[5]  # fast, slow periods are all in nano second
        self.slow_prd = rec[6]
        self.fast_angle = rec[7]
        self.slow_angle = rec[8]
        self.pulse = rec[9]
        self.distance = rec[10]
        self.polyAngle = rec[11]
        self.bowlangle = rec[12]
        self.gx = rec[13]
        self.gy = rec[14]
        self.gz = rec[15]
        self.intensity = rec[16]
        self.intensity_var = rec[17]
        self.ref_intensity = rec[18]
        self.ref_intensity_var = rec[19]
        self.reflectance = rec[20]
        self.raw_data = rec[21:36]
        self.ref_raw_data = rec[36:]
        self.raw = rec
        # print(self.frame, self.channel, self.intensity, self.raw_data)
        if self.intensity != sum(self.raw_data):
            # print("raw nom", self.intensity, self.raw_data)
            pass
        else:
            # print("raw match")
            pass

    @staticmethod
    def get_nth_raw_record(data, nth):
        start = nth * raw_record_in_file_size
        rec = struct.unpack('<21d32H', data[start:start + raw_record_in_file_size])
        return RawRecord(rec)
