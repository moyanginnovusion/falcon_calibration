import math
import numpy as np


def calcReflBeam(debug_level,theta,phi, distance,channel,frame, params):




    N1 = np.array([1.0, 0.0, 0.0])
    N2 = np.array([0.0, 0.0, 0.0])
    N2_temp = np.array([0.0,0.0,0.0])
    N1cur = np.array([0.0,0.0,0.0])

    N2cur = np.array([0.0,0.0,0.0])
    d1 = np.array([0.0,0.0,0.0])
    # cos_theta, sin_theta, cos_phi, sin_phi;
    # double cos_tilt, sin_tilt, cos_shift, sin_shift;
    # double tilt, shift, shift_l, shift_r, temp;
    # int index;
    # double alpha, gamma, cg, sg, ca, sa;
    OUTGOING_LASER_ANGLE = 6.68
    alpha = params.alpha[channel]
    gamma = params.gamma[channel]
    cg = math.cos(math.radians(OUTGOING_LASER_ANGLE+gamma))
    sg = math.sin(math.radians(OUTGOING_LASER_ANGLE+gamma))
    ca = math.cos(math.radians(alpha))
    sa = math.sin(math.radians(alpha))
    d0 = np.array([sg, cg*sa, cg*ca])

    if channel == 0:
        distance = distance - params.t_distance_correction
    elif channel == 1:
        distance = distance - params.b_distance_correction
    elif channel == 2:
        distance = distance - params.b2_distance_correction
    elif channel == 3:
        distance = distance - params.b3_distance_correction

    POLYGON_SURFACE_ANGLE = 25.0
    POLYGON_AXIS_ANGLE = 4.0
    N2[0] = -math.sin(math.radians(POLYGON_SURFACE_ANGLE))
    N2[1] = 0
    N2[2] = math.cos(math.radians(POLYGON_SURFACE_ANGLE))

    cos_theta = math.cos(math.radians(theta))
    sin_theta = math.sin(math.radians(theta))
    cos_phi = math.cos(math.radians(phi))
    sin_phi = math.sin(math.radians(phi))

    cos_polygon_axis = math.cos(math.radians(POLYGON_AXIS_ANGLE))
    sin_polygon_axis = math.sin(math.radians(POLYGON_AXIS_ANGLE))

    # index = (frame + ((channel == 0) ? 0: 2)) & 0x3;

    N1cur[0] = N1[0]*cos_phi + N1[2]*sin_phi
    N1cur[1] = N1[1]
    N1cur[2] = -N1[0]*sin_phi + N1[2]*cos_phi


    N2_temp[0] = N2[0]
    N2_temp[1] = N2[1]*cos_theta + N2[2]*sin_theta
    N2_temp[2] = -N2[1]*sin_theta + N2[2]*cos_theta

    N2cur[0] = N2_temp[0]*cos_polygon_axis + N2_temp[2]*sin_polygon_axis
    N2cur[1] = N2_temp[1]
    N2cur[2] = -N2_temp[0]*sin_polygon_axis + N2_temp[2]*cos_polygon_axis

    temp = N1cur[0] * d0[0] + N1cur[1] * d0[1] + N1cur[2] * d0[2]
    d1[0] = d0[0] - 2 * temp * N1cur[0]
    d1[1] = d0[1] - 2 * temp * N1cur[1]
    d1[2] = d0[2] - 2 * temp * N1cur[2]

    v = np.array([0.0,0.0,0.0])


    temp = N2cur[0] * d1[0] + N2cur[1] * d1[1] + N2cur[2] * d1[2]
    #print(N2cur)
    v[0] = d1[0] - 2 * temp * N2cur[0]
    v[1] = d1[1] - 2 * temp * N2cur[1]
    v[2] = d1[2] - 2 * temp * N2cur[2]
    #print('check v', v[0],v[1],v[2])

    coord = np.array([0.0,0.0,0.0])
    coord[0] = distance * v[0]
    coord[1] = distance * v[1]
    coord[2] = distance * v[2]

    if channel == 1:
        coord[0] = coord[0] + params.h_adjustment
    elif channel == 2:
        coord[0] = coord[0] + params.h2_adjustment
    elif channel == 3:
        coord[0] = coord[0] + params.h3_adjustment


    # if (debug_level > 1) {
    # fprintf(stderr, "C calcReflBeam dist=%f channel=%d, x=%f, y=%f, z=%f\r\n",
    #         distance, channel, coord[0], coord[1], coord[2]);
    # }
    return coord
