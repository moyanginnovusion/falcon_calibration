import json


class FilterPoint:
    def __init__(self, idx, json_data):
        self.idx = idx
        self.x = json_data["y"]
        self.y = json_data["x"]

    def is_neighbor(self, x, y, radius):
        return (x > self.x - radius) and (x < self.x + radius) and (y > self.y - radius) and (y < self.y + radius)

    def dump(self):
        print("point", self.idx, self.x, self.y)


class FilterFrame:
    def __init__(self, idx, json_data):
        self.idx = idx
        self.centers = []
        centers = json_data["centers"]
        for i, c in enumerate(centers):
            self.centers.append(FilterPoint(i, c))
        self.bad_seqs = json_data.get("bad_seqs", None)
        self.good_seqs = json_data.get("good_seqs", None)

    def is_neighbor_of_centers(self, x, y, radius, which_points=None):
        for idx, c in enumerate(self.centers):
            if which_points and idx not in which_points:
                continue
            if c.is_neighbor(x, y, radius):
                return True, idx
        return False, idx

    def dump(self):
        print("frame", self.idx, "bad_seqs", self.bad_seqs, "good_seqs", self.good_seqs)
        for c in self.centers:
            c.dump()


class FilterChannel:
    def __init__(self, idx, json_data):
        self.idx = idx
        self.frames = []
        fs = json_data["frames"]
        for i, f in enumerate(fs):
            self.frames.append(FilterFrame(i, f))
        self.radius = json_data["radius"]

    def dump(self):
        print("channel", self.idx, "radius:", self.radius)
        for f in self.frames:
            f.dump()


class FilterConfigure:
    def __init__(self, json_data):
        self.channels = []
        cs = json_data["channels"]
        for i, c in enumerate(cs):
            self.channels.append(FilterChannel(i, c))

    def dump(self):
        for c in self.channels:
            c.dump()


def main():
    filename = "configures/filter_configure_test.json"
    with open(filename) as data_file:
        data = json.load(data_file)
        a = FilterConfigure(data)
        a.dump()


if __name__ == "__main__":
    main()
