# /usr/bin/python3

import json
import numpy
import matplotlib.pyplot as plt

from data_cursor import DataCursor
from filter_configure import FilterConfigure
from raw_record import raw_record_in_file_size, RawRecord
from mpl_toolkits.mplot3d import Axes3D


def main(args):
    font = {'family': 'serif',
            'color': 'darkred',
            'weight': 'normal',
            'size': 8,
            }

    with open(args.configure) as data_file:
        data = json.load(data_file)
        conf = FilterConfigure(data)
        channel_conf = conf.channels[args.channel]
        frame_conf = channel_conf.frames[args.frame]

    with open(args.input_prefix, 'rb') as FH:
        data = FH.read(args.inputsize)
    total_recs = int(len(data) / raw_record_in_file_size)
    print("Total records found: {}".format(total_recs))

    if not args.draw_each:
        outfile = "{}_c_{}_f_{}.txt".format(args.output_prefix, args.channel, args.frame)
        ofh = open(outfile, 'w')
    else:
        print("draw each")
    x = []
    y = []
    z = []
    intensity = []
    last_frame = -1
    last_channel = -1
    total_frames = 0
    selected_found = 0
    saved = 0
    picked_points = []
    for i in range(0, total_recs):
        rec = RawRecord.get_nth_raw_record(data, i)

        if rec.channel != args.channel:
            continue

        if args.facet >= 0 and rec.facet != args.facet:
            continue

        if rec.frame != last_frame or rec.channel != last_channel:  # found a new frame
            total_frames += 1
            # print("New frame found : c={} f={}->{}, total_frame={}".format(rec.channel, last_frame, rec.frame, total_frames))

            if last_frame == args.frame and last_channel == args.channel:
                selected_found += 1
                if selected_found <= args.nskip:
                    print("\tTarget frame skipped: {}".format(selected_found))
                else:
                    saved += 1
                    print("Total_frames = {} \tSaved = {}".format(total_frames, saved))
                    if saved > args.nsave:
                        break
                    else:
                        if args.draw_each and saved > 1:
                            if args.manual_pick:
                                fig = plt.figure()
                                ax = fig.add_subplot(1, 1, 1)
                                scat = ax.scatter(y, z, s=2)
                                d = DataCursor(scat, y, x)
                                plt.grid()
                                plt.show()
                                picked_points += d.get_picked()
                            else:
                                fig = plt.figure(saved)
                                ax = fig.gca()
                                ax.set_xticks(numpy.arange(-2, 2, 0.1))
                                ax.set_yticks(numpy.arange(-2, 2., 0.1))
                                plt.grid()
                                plt.plot(y, x, c='b', marker='o', linestyle='None', markersize=1)
                                plt.title("Total = " + str(total_frames - 1) + "    frame = " + str(last_frame),
                                          fontdict=font)
                                figname = "{}_channel_{}_frame_{}_{}.jpg".format(args.output_prefix,
                                                                                 int(last_channel),
                                                                                 int(last_frame), total_frames - 1)
                                plt.savefig(figname, dpi=250)
                                plt.close(saved)
                                print("\t {} file saved {} {}/{} points".format(i, figname, len(x), len(y)))
                            x = []
                            y = []
                            z = []
            last_frame = rec.frame
            last_channel = rec.channel

        if rec.frame == args.frame and rec.channel == args.channel and saved > 0:
            if args.draw_each:
                x.append(float(rec.gx))
                y.append(float(rec.gy))
                z.append(float(rec.gz))
                # print("add x[{}]. c={} f={}".format(len(x)-1, rec.channel, rec.frame))
                pass
            else:
                if frame_conf.good_seqs is not None and total_frames not in frame_conf.good_seqs:
                    print("not in good_seqs, skip {}".format(total_frames))
                elif frame_conf.bad_seqs is not None and total_frames in frame_conf.bad_seqs:
                    print("in bad_seqs, skip {}".format(total_frames))
                else:
                    selected = True
                    if not args.nofilter:
                        selected, c_idx = frame_conf.is_neighbor_of_centers(rec.gx, rec.gy, channel_conf.radius, args.which_points)
                        # selected = frame_conf.is_neighbor_of_centers(rec.gx, rec.gy, channel_conf.radius, args.which_points)

                    if selected and args.max_intensity:
                        if rec.intensity > args.max_intensity:
                            print("WARNING: exceed max_intensity, filter x={} y={} z={} inten={}",
                                  rec.gx, rec.gy, rec.gz, rec.intensity)
                            selected = False
                    if selected and args.min_intensity:
                        if rec.intensity < args.min_intensity:
                            print("WARNING: exceed min_intensity, filter x={} y={} z={} inten={}",
                                  rec.gx, rec.gy, rec.gz, rec.intensity)
                            selected = False

                    if selected and args.min_dist:
                        if rec.distance < args.min_dist:
                            print("WARNING: below min_distance, filter x={} y={} z={} inten={}",
                                  rec.gx, rec.gy, rec.gz, rec.distance)
                            selected = False

                    if selected and args.max_dist:
                        if rec.distance > args.max_dist:
                            print("WARNING: exceeded max_distance, filter x={} y={} z={} inten={}",
                                  rec.gx, rec.gy, rec.gz, rec.distance)
                            selected = False

                    if selected and args.zy_ratio:
                        if abs(rec.gz/rec.gy - args.zy_ratio) > 0.07: 
                            print("WARNING: outside zy_ratio range, filter x={} y={} z={}",
                                  rec.gx, rec.gy, rec.gz)
                            selected = False
                            	
                    if selected:
                        #area = 0
                        #total = 0
                        #for i in range(17, 17 + 16):
                        #    area += rec.raw[i] * (i - 16)
                        #    total += rec.raw[i]
                        #wtc = area / float(total)
                        ofh.write(str(total_frames) + "\t" + str(args.starting_cluster + c_idx) + "\t" + "\t".join(str(s) for s in rec.raw) + '\n')
                        if rec.intensity != sum(rec.raw_data):
                            # print("write nom", rec.intensity, rec.raw_data)
                            pass
                        else:
                            # print("write match")
                            pass
                        x.append(float(rec.gx))
                        y.append(float(rec.gy))
                        z.append(float(rec.gz))
                        intensity.append(float(rec.intensity))

    if not args.draw_each:
        ofh.close()
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # plt.plot(y, x, c='r', marker='o', linestyle='None', s=intensity)
        s = [i / 100 for i in intensity]
        #ax.scatter(y, x, marker='o', c='b', s=s)
        scat = ax.scatter(y[::10], z[::10],x[::10], s=2)
        ax.set_xlabel('Y Label')
        ax.set_ylabel('Z Label')
        ax.set_zlabel('X Label')
        ax.set_title("Channel: {} Bowl: {}".format(args.channel, args.frame))
        # d = DataCursor(scat, y, x)
        # print('checkkkkkkkkk')

        plt.grid()
        plt.show()
        fig.savefig("Channel_{}_Bowl_{}".format(args.channel, args.frame)+".png")
        plt.close()
        # print('finishhhhhhh')
        # picked_points += d.get_picked()

    if args.manual_pick:
        picked_string = ", ".join('{{"x": {x:0.2f}, "y": {y:0.2f}}}'.format(x=i[0], y=i[1]) for i in picked_points)
        print(picked_string)

    print("All data processed")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--channel', type=int, required=True)  # select this channel for output only, e.g., 0, 1
    parser.add_argument('--frame', type=int, required=True)  # select this frame for output only, e.g., 0, 1, 2, 3
    parser.add_argument('--facet', type=int, required=False, default=-1)
    parser.add_argument('--configure', type=str, required=True)
    parser.add_argument('--input-prefix', type=str, required=True)
    parser.add_argument('--output-prefix', type=str, required=True)
    parser.add_argument('--nskip', type=int, default=0)  # skip the first n frames (counting only the selected frame ID)
    parser.add_argument('--nsave', type=int, default=100)  # save this many frames
    parser.add_argument('--max-intensity', type=int, default=0)
    parser.add_argument('--min-intensity', type=int, default=0)
    parser.add_argument('--max-dist', type=float, default=250.0)
    parser.add_argument('--min-dist', type=float, default=1.0)
    parser.add_argument('--zy-ratio', type=float, default=0)
    parser.add_argument('--which-points', type=int, nargs='+', default=[])
    parser.add_argument('--nofilter', action="store_true", default=False)
    parser.add_argument('--draw-each', action="store_true", default=False)
    parser.add_argument('--manual-pick', action="store_true", default=False)
    parser.add_argument('--inputsize', type=int, default=2000000000)
    parser.add_argument('--starting-cluster', type=int, default=0)  # starting cluster index

    margs = parser.parse_args()
    main(margs)
