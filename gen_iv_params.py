import json
import yaml
import numpy


class IvParams(object):
    pass


class GenIvParamsItem:
    def __init__(self, data):
        assert isinstance(data, dict), "{} is not dict".format(data)
        self.name = data["name"]
        self.type = data["type"]
        assert self.type in ["int", "float", "double"], "unknown type {}".format(self.type)
        self.array_size = int(data.get("array_size", 1))
        self.postfix_list = data.get("postfix", [''])
        assert isinstance(self.postfix_list, list), "{} is not a list".format(self.postfix_list)
        self.postfix_list.sort()
        self.func = int if self.type is "int" else float
        self.init_v = data.get("init_v", 0)
        self.min_v = data.get("min_v", self.init_v)
        self.max_v = data.get("max_v", self.init_v)
        self.fixed = data.get("fixed", True)

    def get_v(self, name, idx):
        a = getattr(self, name)
        if isinstance(a, list):
            assert self.array_size > 1
            assert isinstance(idx, int)
            return self.func(a[idx])
        else:
            return self.func(a)


    def dump(self):
        print("name={}, type={}, array_size={}, init_v={}, max_v={}, min_v={}, fixed={}"\
            .format(self.name, self.type, self.array_size, self.init_v, self.max_v, self.min_v, self.fixed))


class GenIvParams:
    def __init__(self, data):
        self.items = []
        paras = data.get("parameters", [])
        for i in paras:
            self.items.append(GenIvParamsItem(i))
        self.items.sort(key=lambda x: x.name, reverse=False)

    def dump(self):
        for i in self.items:
            i.dump()

    def get_array_for_optimizer(self):
        p = []
        bound = ()
        name = []
        for item in self.items:
            if item.fixed:
                print("skip fixed", item.name)
                pass
            else:
                for _ in item.postfix_list:
                    for i in range(item.array_size):
                        p.append(item.get_v("init_v", i))
                        bound = bound + ((item.get_v("min_v", i), item.get_v("max_v", i)), )
                        name.append(item.name)
        return p, bound, name

    def get_iv_params(self, p):
        m = self.get_iv_params_map(p)
        ret = IvParams()
        for k in m:
            setattr(ret, k, m[k])
        return ret

    def get_iv_params_json_for_next_stage(self, p):
        m = self.get_iv_params_list(p)
        ret = []
        for i in m:
            item = i["item"]
            r = {}
            r["name"] = i["name"]
            r["type"] = item.type
            r["array_size"] = item.array_size
            r["init_v"] = i["value"]
            r["fixed"] = item.fixed
            r["max_v"] = item.max_v
            r["min_v"] = item.min_v
            ret.append(r)

        return {"parameters" :ret}

    def get_iv_params_map(self, p):
        m = self.get_iv_params_list(p)
        ret = {}
        for i in m:
            ret[i["name"]] = i["value"]
        return ret

    def get_iv_params_list(self, p):
        if isinstance(p, numpy.ndarray):
            p = p.tolist()
        ret = []
        idx = 0
        for item in self.items:
            for postfix in item.postfix_list:
                name = item.name + ("_" if len(item.postfix_list) > 1 else "")
                name += postfix
                if item.array_size > 1:
                    ar = []
                    for i in range(item.array_size):
                        if item.fixed:
                            value = item.get_v("init_v" , i)
                        else:
                            value = p[idx]
                            idx += 1
                        ar.append(value)
                else:
                    if item.fixed:
                        ar = item.get_v("init_v", None)
                    else:
                        ar = p[idx]
                        idx += 1
                rec = {}
                rec["name"] = name
                rec["value"] = ar
                rec["item"] = item
                ret.append(rec)

        assert idx == len(p), "len mismatch len({}) != {}".format(p, idx)
        return ret

    def write_to_yaml(self, file, varible_list):
        with open(file, 'w') as out:
            data = self.get_iv_params_map(varible_list)
            yaml.dump(data, out, default_flow_style=False)

    def write_to_json_for_next_stage(self, file, varible_list):
        with open(file, 'w') as out:
            data = self.get_iv_params_json_for_next_stage(varible_list)
            json.dump(data, out, sort_keys=True, indent=4)


    def test(self):
        self.dump()
        a, _ = self.get_array_for_optimizer()
        c = self.get_iv_params(a)
        print(c.__dict__)


def main(args):
    with open(args.json) as data_file:
        data = json.load(data_file)
        ivs = GenIvParams(data)
        ivs.test()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--json', type=str, default="test_parameters.json")

    margs = parser.parse_args()
    main(margs)
