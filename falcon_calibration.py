import pandas as pd
import numpy as np
import json
from gen_iv_params import GenIvParams
from falcon_model import calcReflBeam
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import minimize
from plane_fit_func import plane_fit_func
from datetime import datetime
import time

def dump_yaml_for_rviz(p, outfile):
    block_1 = \
    '''
    !!python/object/new:dynamic_reconfigure.encoding.Config
    dictitems:
      b2_alpha: {}
      b2_gamma: {}
      b2_temp_vbr0: 30
      b2_vbr0: 44000
      b3_alpha: {}
      b3_gamma: {}
      b3_temp_vbr0: 30
      b3_vbr0: 44000
      b_alpha: {}
      b_gamma: {}
      b_temp_vbr0: 30
      b_vbr0: 44000
      bottom2_dist_correction: {}
      bottom3_dist_correction: {}
      bottom_dist_correction: {}
      firing_cycle: 2
      fov_top_high_angle: 10.0
      fov_top_left_angle: -55.0
      fov_top_low_angle: -15.0
      fov_top_right_angle: 55.0
      galvo_center_angle: 73.5
      galvo_scan_range: 20.0
      groups: !!python/object/new:dynamic_reconfigure.encoding.Config
    '''.format( round(p.alpha[2],6), \
        round(p.gamma[2],6), \
        round(p.alpha[3],6), \
        round(p.gamma[3],6), \
        round(p.alpha[1],6), \
        round(p.gamma[1],6), \
        round(p.b2_distance_correction,6), \
        round(p.b3_distance_correction,6), \
        round(p.b_distance_correction,6) \
    )
    # print (block_1)
    block_2 = \
    '''
        dictitems:
          b2_alpha: {}
          b2_gamma: {}
          b2_temp_vbr0: 30
          b2_vbr0: 44000
          b3_alpha: {}
          b3_gamma: {}
          b3_temp_vbr0: 30
          b3_vbr0: 44000
          b_alpha: {}
          b_gamma: {}
          b_temp_vbr0: 30
          b_vbr0: 44000
          bottom2_dist_correction: {}
          bottom3_dist_correction: {}
          bottom_dist_correction: {}
          firing_cycle: 2
          fov_top_high_angle: 10.0
          fov_top_left_angle: -55.0
          fov_top_low_angle: -15.0
          fov_top_right_angle: 55.0
          galvo_center_angle: 73.5
          galvo_scan_range: 20.0
          groups: !!python/object/new:dynamic_reconfigure.encoding.Config
    '''.format( round(p.alpha[2],6), \
        round(p.gamma[2],6), \
        round(p.alpha[3],6), \
        round(p.gamma[3],6), \
        round(p.alpha[1],6), \
        round(p.gamma[1],6), \
        round(p.b2_distance_correction,6), \
        round(p.b3_distance_correction,6), \
        round(p.b_distance_correction,6) \
    )
    block_3 = \
    '''
            state: []
          height2_adj: 0.0
          height3_adj: 0.0
          height_adj: 0.0
          max_distance: 280.0
          max_intensity: 3500
          min_distance: 2.3
          p_offset: 76.0
          parameters: !!python/object/new:dynamic_reconfigure.encoding.Config
            state: []
          published_frames: 1
          remove_gap: 0
          t_alpha: {}
          t_gamma: {}
          t_temp_vbr0: 30
          t_vbr0: 44000
          temp_vbr0: 30
          top_dist_correction: {}
          type: ''
        state: []
      height2_adj: 0.0
      height3_adj: 0.0
      height_adj: 0.0
      max_distance: 280.0
      max_intensity: 3500
      min_distance: 2.3
      p_offset: 76.0
      published_frames: 1
      remove_gap: 0
      t_alpha: {}
      t_gamma: {}
      t_temp_vbr0: 30
      t_vbr0: 44000
      temp_vbr0: 30
      top_dist_correction: {}
    state: []
    '''.format( \
        round(p.alpha[0],6), \
        round(p.gamma[0], 6),\
        round(p.t_distance_correction, 6), \
        round(p.alpha[0], 6), \
        round(p.gamma[0], 6), \
        round(p.t_distance_correction,6) \
    )
    ofh = open(outfile,"w")
    ofh.write(block_1)
    ofh.write(block_2)
    ofh.write(block_3)
    ofh.close()

def find_pyramid_tip(df):
    planes_coeffs, outlier_Endlist, outliers_coord, outliers_dists, inlier_list = plane_fit_func(df, threshold=0.02, show_plot=False, color='b', max_planes=3)
    M = planes_coeffs.values.transpose()
    M_det = np.linalg.det(M[:, :3])
    x = np.concatenate((M[:, 3].reshape(-1, 1), M[:, 1].reshape(-1, 1), M[:, 2].reshape(-1, 1)), axis=1)
    y = np.concatenate((M[:, 0].reshape(-1, 1), M[:, 3].reshape(-1, 1), M[:, 2].reshape(-1, 1)), axis=1)
    z = np.concatenate((M[:, 0].reshape(-1, 1), M[:, 1].reshape(-1, 1), M[:, 3].reshape(-1, 1)), axis=1)
    x_intersec = np.linalg.det(x) / (-M_det)
    y_intersec = np.linalg.det(y) / (-M_det)
    z_intersec = np.linalg.det(z) / (-M_det)
    print(z_intersec)
    return x_intersec, y_intersec, z_intersec

def final_cost_func(opt_params, gen_iv_params,output_tips):

    params = gen_iv_params.get_iv_params(opt_params)
    new_tips = np.zeros((4, 3))

    for channel in range(4):
        result = calcReflBeam(0, output_tips.iloc[channel]['polygon_angle'], output_tips.iloc[channel]['bowl_angle'], output_tips.iloc[channel]['distance'], int(channel),
                              0, params)
        new_tips[channel, 0] = result[0]
        new_tips[channel, 1] = result[1]
        new_tips[channel, 2] = result[2]
    final_error1 = np.sum(np.abs(new_tips[1] - new_tips[0])) / 3
    final_error2 = np.sum(np.abs(new_tips[2] - new_tips[0])) / 3
    final_error3 = np.sum(np.abs(new_tips[3] - new_tips[0])) / 3
    print(round((final_error1 + final_error2 + final_error3) / 3, 7))
    return (final_error1 + final_error2 + final_error3) / 3


def find_all_tips_cali(args):
    intersection_df = pd.DataFrame(columns=['name', 'x', 'y', 'z', 'bowl#', 'chan#', 'distance', 'bowl_angle', 'polygon_angle', 'position'])
    for channel_num in range(4):
        if channel_num == 0:
            txt_name = args.device_id + '_cali_c_0_f_0.txt'
        elif channel_num == 1:
            txt_name = args.device_id + '_cali_c_1_f_0.txt'
        elif channel_num == 2:
            txt_name = args.device_id + '_cali_c_2_f_0.txt'
        else:
            txt_name = args.device_id + '_cali_c_3_f_0.txt'

        data_all = pd.read_csv(args.input_dir + '/' + txt_name, header=None, sep='\t')

        # Drop non used columns in dataset
        data_drop = data_all.drop([5, 6, 7, 8], axis=1)  ## CHANNEL 0 DATA - BF0
        data_ready = data_drop.iloc[:, :19]
        input_data = data_ready

        # Rename data columns
        input_data.columns = ['frame_idx', 'cluster_idx', 'bowl_idx', 'channel_idx', 'polygon_facet_idx',
                              'polygon_encoder',
                              'bowl_encoder', 'time_stamp', 'distance', 'polygon_angle', 'bowl_angle', 'x', 'y',
                              'z',
                              'ref_intensity',
                              'ref_intensity_std', 'return_signal_intensity', 'return_signal_intensity_std',
                              'reflectance']
        print('before',len(input_data))
        print(len(np.unique(input_data['frame_idx'])))
        # input_data = input_data[input_data['frame_idx'] > nskip]
        # input_data = input_data[input_data['frame_idx'] < (nskip+nsave)]
        print('after', len(input_data))
        points_for_fitting = input_data[['x','y','z']]
        x, y, z = find_pyramid_tip(points_for_fitting.values[:, [0, 1, 2]])
        intersection_df = intersection_df.append(
            {'name': 0, 'x': x, 'y': y, 'z': z,
             'bowl#': 0, 'chan#': channel_num, 'distance': 0, 'bowl_angle': 0, 'polygon_angle': 0,
             'position': 0}, ignore_index=True)
    print(intersection_df)
    return intersection_df

def find_close_point(fit_points,args):


    df_list = []
    for channel_num in range(4):
        if channel_num == 0:
            txt_name = args.device_id + '_cali_c_0_f_0.txt'
        elif channel_num == 1:
            txt_name = args.device_id + '_cali_c_1_f_0.txt'
        elif channel_num == 2:
            txt_name = args.device_id + '_cali_c_2_f_0.txt'
        else:
            txt_name = args.device_id + '_cali_c_3_f_0.txt'

        data_all = pd.read_csv(args.input_dir + '/' + txt_name, header=None, sep='\t')

        # Drop non used columns in dataset
        data_drop = data_all.drop([5, 6, 7, 8], axis=1)  ## CHANNEL 0 DATA - BF0
        data_ready = data_drop.iloc[:, :19]
        input_data = data_ready

        # Rename data columns
        input_data.columns = ['frame_idx', 'cluster_idx', 'bowl_idx', 'channel_idx', 'polygon_facet_idx',
                              'polygon_encoder',
                              'bowl_encoder', 'time_stamp', 'distance', 'polygon_angle', 'bowl_angle', 'x', 'y',
                              'z',
                              'ref_intensity',
                              'ref_intensity_std', 'return_signal_intensity', 'return_signal_intensity_std',
                              'reflectance']

        df_list.append(input_data)


    final_tips = pd.DataFrame()
    for idx in range(4):
        all_points = df_list[idx]

        each_tip = []
        each_tip.append(fit_points.iloc[idx]['x'])
        each_tip.append(fit_points.iloc[idx]['y'])
        each_tip.append(fit_points.iloc[idx]['z'])

        diff = np.sqrt((each_tip[0] - all_points['x'])**2 + (each_tip[1] - all_points['y'])**2 + (each_tip[2] - all_points['z'])**2)
        all_points['diff'] = diff
        all_points = all_points.sort_values(by = ['diff'])
        # print(all_points.iloc[0])
        final_tips= final_tips.append(all_points.iloc[0])
    print('diffffffffffffffffffffffffffffff')
    print(final_tips['diff'])
    return final_tips

def main(txt_name, args):
    print(txt_name)
    all_tips = find_all_tips_cali(args)
    closer_points = find_close_point(all_tips, args)

    with open(args.input_json) as data_file:
        data = json.load(data_file)
        ivs = GenIvParams(data)
        ivs.dump()
        param_set, bnds, names = ivs.get_array_for_optimizer()
        params = ivs.get_iv_params(param_set)

    init_guess = np.array(param_set)

    opt_res = minimize(final_cost_func, init_guess, method='L-BFGS-B', bounds=bnds,
                       # options={'eps':2e-5, 'ftol':1e-6, 'maxcor':45},
                       # options={'ftol': 1e-7},
                       args=(ivs, closer_points))
    params = ivs.get_iv_params(opt_res.x)
    print('alpha',params.alpha)
    print('gamma', params.gamma)
    print('h_adjustment', params.h_adjustment)
    print('h2_adjustment', params.h2_adjustment)
    print('h3_adjustment', params.h3_adjustment)
    print('t_distance_correction',params.t_distance_correction)
    print('b_distance_correction', params.b_distance_correction)
    print('b2_distance_correction', params.b2_distance_correction)
    print('b3_distance_correction', params.b3_distance_correction)
    return params


if __name__ == '__main__':
    import argparse

    start_time = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', type=str, required=False, default='F2TEST')  #
    parser.add_argument('--input_json', type=str, required=False, default='F2TEST_input.json')
    parser.add_argument('--device_id', type=str, required=False, default='F2TEST')
    args = parser.parse_args()

    txt_name0 = './filter/'+ args.device_id + '_cali_c_0_f_0.txt'
    txt_name1 = './filter/'+ args.device_id + '_cali_c_1_f_0.txt'
    txt_name2 = './filter/'+ args.device_id + '_cali_c_2_f_0.txt'
    txt_name3 = './filter/'+ args.device_id + '_cali_c_3_f_0.txt'
    txt_name = [txt_name0, txt_name1, txt_name2, txt_name3]
    params = main(txt_name, args)

    dt = datetime.now()
    # json_filename = os.path.basename(args.input_json)
    # base_filename, _ = os.path.splitext(json_filename)
    base_filename = str(params.device_sn) + '_' + dt.strftime('%Y%m%d_%H%M%S')

    dump_yaml_for_rviz(params, args.input_dir + "/" + base_filename + "_internal.yaml")

    print('Time Elapsed: ', round(time.time() - start_time, 1), 'seconds')