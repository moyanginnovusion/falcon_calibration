import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from fit_plane_LSE import fit_plane_LSE_RANSAC
from fit_plane_LSE import get_point_dist
import time

def plane_fit_func(np_df, threshold = 0.05, show_plot = False, color = 'b', max_planes = 1):

    start = time.time()
    # dataFrameXYZ = data_Frame.drop(columns=['frame_idx','flags','intensity','timestamp'])
    dataFrameXYZ = np_df
    dataFrameXYZ1 = np.concatenate((dataFrameXYZ, np.ones((dataFrameXYZ.shape[0], 1))), axis=1)

    to_fit = dataFrameXYZ1
    planes_coeffs = pd.DataFrame()
    outliers_nb = pd.Series()
    plane_id = 0
    min_inliers = 7*len(dataFrameXYZ1)//100 #from 10 to 7 on June 10 - 2019

    # Find planes coefficients and list of outliers
    while ((len(to_fit) > min_inliers) & (plane_id<max_planes)):
        p, inlier_list, outlier_list = fit_plane_LSE_RANSAC(to_fit,100, threshold, True)

        inliers_dists = get_point_dist(to_fit[inlier_list,:], p)
        outliers_dists = get_point_dist(to_fit[outlier_list,:], p)

        to_fit = to_fit[outlier_list, :]

        # Avoid planes fitted with low number of points
        if (len(inlier_list) > min_inliers):
            planes_coeffs[plane_id] = p
            plane_id = plane_id + 1
            # print("Plane id: " + str(plane_id) + ", Coeffs: " + str(np.transpose(p)))

    # Final list and coordinates of outliers
    outlier_Endlist = outlier_list

    outliers_coord = pd.DataFrame(to_fit[:, :-1])
    outliers_coord.columns = ["x", "y", "z"]

    end = time.time()
    comp_time = (end - start)

    # SHOW PLOTS OF DATA / FITTED PLANES / FILTERED OUTLIERS
    if (show_plot == True):
        points = dataFrameXYZ1

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        scatter1 = ax.scatter(data_Frame.z, -data_Frame.y, data_Frame.x, color=color, marker=".", s=20)

# !!!!!!!!!!!!!!!!!!! PLOT PLAN !!!!!!!!!!!!!!!!!!!
        # x = np.arange(np.min(points[:, 0]), np.max(points[:, 0]), 0.1)
        # z = np.arange(np.min(points[:, 2]), np.max(points[:, 2]), 0.1)
        #
        # xx, zz = np.meshgrid(x, z)
        #
        # for plane_id in range(planes_coeffs.shape[1]):
        #     coeffs = planes_coeffs[plane_id]
        #     #yy = (-coeffs[0] * zz - coeffs[2] * xx - coeffs[3]) / coeffs[1]
        #     yy = (-coeffs[0] * xx - coeffs[2] * zz - coeffs[3]) / coeffs[1]
        #
        #
        #     #scatter2 = ax.plot_surface(zz, yy, xx, alpha=0.2, linewidth=0.5, color = color)
        #     scatter2 = ax.plot_surface(xx, yy, zz, alpha=0.2, linewidth=0.5, color = color)

        x = np.arange(np.min(points[:, 0]), np.max(points[:, 0]), 0.1)
        y = np.arange(np.min(points[:, 1]), np.max(points[:, 1]), 0.1)

        xx, yy = np.meshgrid(x, y)

        for plane_id in range(planes_coeffs.shape[1]):
            coeffs = planes_coeffs[plane_id]
            # yy = (-coeffs[0] * xx - coeffs[2] * zz - coeffs[3]) / coeffs[1]
            zz = (-coeffs[0] * xx - coeffs[1] * yy - coeffs[3]) / coeffs[2]

            #scatter2 = ax.plot_surface(xx, yy, zz, alpha=0.2, linewidth=0.5, color = color)
            scatter2 = ax.plot_surface(zz, -yy, xx, alpha=0.2, linewidth=0.5, color = color)


        # Outline outliers on graph
        scatter3 = ax.scatter(outliers_coord.z, -outliers_coord.y, outliers_coord.x,  marker="x", color="r", s=20)

        # # Line intersec
        # scatter4 = ax.plot(line3D[:,0],line3D[:,1],line3D[:,2], color = color)

        # ax.axis('equal')

        # Axis labels
        ax.set_xlabel('Z')
        ax.set_ylabel('Y')
        ax.set_zlabel('X')

        # Legend
        ax.legend([scatter1,  scatter3], ['Dataframe points',  'Outliers' ], numpoints=4,loc=4)

         # Figure Title
        plt.title("Target distance " + str((round(np.median(data_Frame.z), 0))) +"m" + "\n Dataframe "+ str(int(np.unique(data_Frame.frame_idx))) + " / Timestamp "+ str(np.ceil(round(np.median(data_Frame.timestamp), 4))) , fontsize=12 )
        plt.show(block=True)

    return planes_coeffs, outlier_Endlist, outliers_coord, outliers_dists, inlier_list  #comp_time

# + "\n Compute Time : " +str(round(comp_time, 4))+ " sec"